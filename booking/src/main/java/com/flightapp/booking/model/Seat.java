package com.flightapp.booking.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.flightapp.booking.constants.SeatType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Seat {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	Long id;
	private SeatType seatType; 
	private	int seatNumber;
	private boolean busy;
}
