package com.flightapp.booking.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Plane {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	Long id;
	private String name;
	private String model;
	private List<Seat> seats;
	private Airline airline;

}
