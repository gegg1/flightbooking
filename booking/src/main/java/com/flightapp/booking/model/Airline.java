package com.flightapp.booking.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Airline {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	Long id;
	private String name;
	private String logo;
	private boolean status;
	

}
