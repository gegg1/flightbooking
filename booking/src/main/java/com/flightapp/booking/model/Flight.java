package com.flightapp.booking.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Flight {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	Long id;
	private Place fromPlace;
	private Place toPlace;
	private boolean oneWay;
	private Date departureDate;
	private long flightNumber;
	private Plane plane;
	private Place returnPlace;
	private Date returnDate;
	

}
