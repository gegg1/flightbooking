package com.flightapp.booking.constants;

public enum SeatType {
	
	FIRST_CLASS, SECOND_CLASS, THIRD_CLASS;

}
