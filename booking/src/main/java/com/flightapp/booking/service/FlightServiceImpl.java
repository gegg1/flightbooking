package com.flightapp.booking.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flightapp.booking.model.Flight;
import com.flightapp.booking.model.Place;
import com.flightapp.booking.repository.FlightRepository;

@Service
public class FlightServiceImpl implements FlightService {
	
	@Autowired
	FlightRepository flightRepository;

	@Override
	public Flight searchOneWayFlight(String from, String to, Date dateFrom) {
		Place fromPlace = new Place();
		fromPlace.setCountry(from);
		
		Place toPlace = new Place();
		toPlace.setCountry(to);
		
		return flightRepository.findByFromPlaceAndToPlaceAndDepartureDateAndIsOneWay(fromPlace, toPlace, dateFrom);
	}

	@Override
	public Flight searchRoundTrip(Place from, Place to, Date dateFrom, Place returnPlace, Date returnDate) {
		// TODO Auto-generated method stub
		return null;
	}

}
