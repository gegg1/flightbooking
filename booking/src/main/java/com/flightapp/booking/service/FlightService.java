package com.flightapp.booking.service;

import java.util.Date;

import com.flightapp.booking.model.Flight;
import com.flightapp.booking.model.Place;

public interface FlightService {
	
	Flight searchOneWayFlight(String from, String to, Date dateFrom);
	
	Flight searchRoundTrip(Place from, Place to, Date dateFrom, Place returnPlace, Date returnDate);
	

}
