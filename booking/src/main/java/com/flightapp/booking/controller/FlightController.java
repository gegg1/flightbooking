package com.flightapp.booking.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flightapp.booking.model.Flight;
import com.flightapp.booking.service.FlightService;

@RestController
@RequestMapping("/flight")
public class FlightController {
	
	@Autowired
	FlightService flightService;
	
	@GetMapping("/search")
	public Flight searchOneWayFlight(@RequestParam String from, @RequestParam String to, @RequestParam Date dateFrom) {
		return flightService.searchOneWayFlight(from, to, dateFrom);
	}

}
