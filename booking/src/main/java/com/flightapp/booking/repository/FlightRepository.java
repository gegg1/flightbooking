package com.flightapp.booking.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flightapp.booking.model.Flight;
import com.flightapp.booking.model.Place;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long>{
	
	Flight findByFromPlaceAndToPlaceAndDepartureDateAndIsOneWay(Place fromPlace, Place toPlace, Date departureDate);

}
